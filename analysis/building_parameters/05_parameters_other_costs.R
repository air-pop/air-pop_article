
#This file set up the parameters related to the other cost shared by the two strategies

# clean environment----
rm(list = ls())

# packages importation----
library(data.table)

# create table for recording list parameters----
po <- data.table()

# telephone cost----
po <- rbind(po,
            data.table(
              Category = "Other cost related data",
              IndicatorName = "Telephone cost",
              IndicatorCode = "TelephoneCost",
              Type = "Cost (euros)",
              Value = 16,
              ValueSD = NA,
              ValueMin = NA,
              ValueMax = NA,
              ValueUnit = " ",
              Distribution = "Point estimate"
            )
)

po <- rbind(po,
            data.table(
              Category = "Other cost related data",
              IndicatorName = "Telephone plan cost per year",
              IndicatorCode = "TelephonePlanCost",
              Type = "Cost (euros)",
              Value = 24,
              ValueSD = NA,
              ValueMin = NA,
              ValueMax = NA,
              ValueUnit = " ",
              Distribution = "Point estimate"
            )
)

# save parameters----
save(po, file = "data/temp/parameters_other_costs.rData")