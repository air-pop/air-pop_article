---
title: "07_simulation_cost"
author: "MI"
date: "05/11/2021"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
# clean environment----
rm(list = ls())
memory.limit(100000)

# packages importation----
library(mcmc)
library(msm)
library(plyr)
library(dplyr)
library(data.table)
library(labelled)
library(stats)
library(mc2d)
library(questionr)
library(ggplot2)
library(tidyr)
library(lubridate)
library(stringr)
library(scales)
library(boot)
library(readxl)
library(cowplot)


# import data----
cesy <- readRDS(paste0(getwd(),"/data/temp/simulation_cost_effectiveness.rds"))
load(paste0(getwd(),"/data/temp/care_pathway_data.rData"))
load(paste0(getwd(),"/data/temp/drone_data.rData"))
load(paste0(getwd(),"/data/temp/motorbike_data.rData"))
load(paste0(getwd(),"/data/temp/parameters_other_costs.rData"))
hop <- data.table(read_excel("data/data_health_center.xlsx"))

```

```{r prepare data}
gdp <- 1194 # PIB in US$

# merge the parameter file----
p <- rbind(pb,pd,pm,po)
rm(pb,pd,pm,po,ARTiniPr,returpb)

```

```{r convert cost to US$}
rt <- 1.156297 # US$ euro per euro, november 2021

cesy <- cesy %>% mutate(DroneHIVCareARTCostr3 = DroneHIVCareARTCostr3*rt,
                        DroneHIVCareARTCostr5 = DroneHIVCareARTCostr5*rt,
                        DroneHIVARTCostr3 = DroneHIVARTCostr3*rt,
                        MBHIVCareARTCostr3 = MBHIVCareARTCostr3*rt,
                        MBHIVCareARTCostr5 = MBHIVCareARTCostr5*rt,
                        BaselineHIVCareARTCostr3 = BaselineHIVCareARTCostr3*rt,
                        BaselineHIVCareARTCostr5 = BaselineHIVCareARTCostr5*rt,
                        MBHIVARTCostr3 = MBHIVARTCostr3*rt,
                        DroneCostTravel = DroneCostTravel*rt,
                        MBCostTravel = MBCostTravel*rt,
                        MBAccidentCost = MBAccidentCost*rt,
                          )

p[Type == "Cost (euros)", Value := Value*rt]
p[Type == "Cost (euros)", Type := "Cost (US$)"]
```


# overall analysis
```{r data for the overall results}
rd <- .03

#cesy <- cesy[RoadDistanceKm >= 20,]

# remove the distance
ces <- cesy[,.(DroneSurvivalDays = sum(DroneSurvivalDays),
              DroneSurvivalDaysr1_5 = sum(DroneSurvivalDaysr1_5),  
              DroneSurvivalDaysr3 = sum(DroneSurvivalDaysr3), 
              DroneSurvivalDaysr5 = sum(DroneSurvivalDaysr5), 
              
              MBSurvivalDays = sum(MBSurvivalDays), 
              MBSurvivalDaysr1_5 = sum(MBSurvivalDaysr1_5), 
              MBSurvivalDaysr3 = sum(MBSurvivalDaysr3), 
              MBSurvivalDaysr5 = sum(MBSurvivalDaysr5), 
              
              BaselineSurvivalDays = sum(BaselineSurvivalDays), 
              BaselineSurvivalDaysr1_5 = sum(BaselineSurvivalDaysr1_5), 
              BaselineSurvivalDaysr3 = sum(BaselineSurvivalDaysr3), 
              BaselineSurvivalDaysr5 = sum(BaselineSurvivalDaysr5), 
              
              DroneHIVCareARTCostr3 = sum(DroneHIVCareARTCostr3),
              DroneHIVCareARTCostr5 = sum(DroneHIVCareARTCostr5),
              DroneHIVARTCostr3 = sum(DroneHIVARTCostr3),
              
              MBHIVCareARTCostr3 = sum(MBHIVCareARTCostr3),
              MBHIVCareARTCostr5 = sum(MBHIVCareARTCostr5),
              MBHIVARTCostr3 = sum(MBHIVARTCostr3),
              
              BaselineHIVCareARTCostr3 = sum(BaselineHIVCareARTCostr3),
              BaselineHIVCareARTCostr5 = sum(BaselineHIVCareARTCostr5),
              BaselineHIVARTCostr3 = sum(BaselineHIVARTCostr3),
              
              DroneCostTravel = sum(DroneCostTravel, na.rm = T),
              DroneLoss = sum(DroneLoss, na.rm = T),
              
              MBCostTravel = sum(MBCostTravel, na.rm = T),
              MBReplacement = sum(MBReplacement),
              TravelTimeDiff = weighted.mean(TravelTimeDiff,
                                             HIVExposedInfant, na.rm = T),
              MBAccidentCost = sum(MBAccidentCost, na.rm = T),
              IsInfantHIVInfected = sum(IsInfantHIVInfected),
              HIVExposedInfant = sum(HIVExposedInfant)
              ),
           by = .(SimulationID,Year)]

# add fixed total annual cost with 3% discount----
## drone
ces[, DroneTotalAnnualCost := (DroneCostTravel+
                                 p[IndicatorCode == "DronePilotCost",Value]*1.15+
                                 p[IndicatorCode == "DroneMainCost",Value]*p[IndicatorCode == "DroneCost",Value]+
                                 p[IndicatorCode == "DroneAssuranceCost",Value]*p[IndicatorCode == "DroneCost",Value]+
                                 DroneLoss*p[IndicatorCode == "DroneCost",Value]+
                                 p[IndicatorCode == "TelephonePlanCost",Value]*35)/((1+rd)^(Year-1))]

## motorbike
ces[, MBTotalAnnualCost := (MBCostTravel+MBAccidentCost+
                              p[IndicatorCode == "BikeDriverSalary",Value]*1.15+
                              p[IndicatorCode == "BikeInsurance",Value]+
                              p[IndicatorCode == "BikeRegistration",Value]+
                              p[IndicatorCode == "BikeCost",Value]*MBReplacement+
                              p[IndicatorCode == "TelephonePlanCost",Value]*35)/((1+rd)^(Year-1))]

## consider HIV care separately
### care and ART cost
ces[,DroneHIVCareARTCostr3 := DroneHIVCareARTCostr3/((1+rd)^(Year-1))]
ces[,MBHIVCareARTCostr3 := MBHIVCareARTCostr3/((1+rd)^(Year-1))]
ces[,BaselineHIVCareARTCostr3 := BaselineHIVCareARTCostr3/((1+rd)^(Year-1))]

### ART only
ces[,DroneHIVARTCostr3 := DroneHIVARTCostr3/((1+rd)^(Year-1))]
ces[,MBHIVARTCostr3 := MBHIVARTCostr3/((1+rd)^(Year-1))]
ces[,BaselineHIVARTCostr3 := BaselineHIVARTCostr3/((1+rd)^(Year-1))]

# create final simulation table-----
ces <- ces[,.( DroneSurvivalDays = sum(DroneSurvivalDays),
               DroneSurvivalDaysr1_5 = sum(DroneSurvivalDaysr1_5),
               DroneSurvivalDaysr3 = sum(DroneSurvivalDaysr3),
               DroneSurvivalDaysr5 = sum(DroneSurvivalDaysr5),
               
               MBSurvivalDays = sum(MBSurvivalDays), 
               MBSurvivalDaysr1_5 = sum(MBSurvivalDaysr1_5),
               MBSurvivalDaysr3 = sum(MBSurvivalDaysr3),
               MBSurvivalDaysr5 = sum(MBSurvivalDaysr5),
               
               BaselineSurvivalDays = sum(BaselineSurvivalDays), 
               BaselineSurvivalDaysr1_5 = sum(BaselineSurvivalDaysr1_5), 
               BaselineSurvivalDaysr3 = sum(BaselineSurvivalDaysr3), 
               BaselineSurvivalDaysr5 = sum(BaselineSurvivalDaysr5), 
               
               DroneHIVCareARTCostr3 = sum(DroneHIVCareARTCostr3),
               MBHIVCareARTCostr3 = sum(MBHIVCareARTCostr3),
               BaselineHIVCareARTCostr3 = sum(BaselineHIVCareARTCostr3),
               
               DroneTotalCost = sum(DroneTotalAnnualCost, na.rm = T),
               MBTotalCost = sum(MBTotalAnnualCost, na.rm = T),
               TravelTimeDiff = weighted.mean(TravelTimeDiff,
                                              HIVExposedInfant, na.rm = T),
               HIVExposedInfant = sum(HIVExposedInfant),
               DroneLoss = sum(DroneLoss),
               
               DroneHIVARTCostr3 = sum(DroneHIVARTCostr3),
               MBHIVARTCostr3 = sum(MBHIVARTCostr3),
               BaselineHIVARTCostr3 = sum(BaselineHIVARTCostr3)
              ),
            by = SimulationID]

ces[,DroneTotalCost := DroneTotalCost + DroneHIVCareARTCostr3 +
      p[IndicatorCode %in% c("DroneCost","DroneFlightCaseCost","DroneConsoleCost",
                             "DronePiloteTrainingOnsiteCost","DronePiloteTrainingOnlineCost"),
        sum(Value)] +
      p[IndicatorCode == "DroneMedicTrainingCost",Value*33*2] +
      p[IndicatorCode == "TelephoneCost",Value]*35]

ces[,MBTotalCost := MBTotalCost + MBHIVCareARTCostr3 +
      p[IndicatorCode %in% c("BikeCost","BikeHelmet"), sum(Value)] +
      p[IndicatorCode == "TelephoneCost",Value]*35]

ces[,BaselineTotalCost := BaselineHIVCareARTCostr3]

ces[,DroneTotalCostWithoutART := DroneTotalCost - DroneHIVCareARTCostr3]
ces[,MBTotalCostWithoutART := MBTotalCost - MBHIVCareARTCostr3]
ces[,BaselineTotalCostWithoutART := BaselineTotalCost - BaselineHIVARTCostr3]

```

```{r ICER}
# create graphic
ces[,mean(DroneTotalCost-MBTotalCost)/mean((DroneSurvivalDaysr3-MBSurvivalDaysr3)/365)]

dt <- rbind(ces[,.(DiffCost = DroneTotalCost-MBTotalCost, 
                   DiffLife = (DroneSurvivalDaysr3-MBSurvivalDaysr3)/365, Comp = "UAV vs Motorcycle")],
            ces[,.(DiffCost = DroneTotalCost-BaselineTotalCost, DiffLife = (DroneSurvivalDaysr3-BaselineSurvivalDaysr3)/365, Comp = "UAV vs Baseline")],
            ces[,.(DiffCost = MBTotalCost-BaselineTotalCost, DiffLife = (MBSurvivalDaysr3-BaselineSurvivalDaysr3)/365, Comp = "Motorcycle vs Baseline")]
            )

dt[,mean(DiffCost)/mean(DiffLife), by = Comp]
dt[,mean(DiffLife), by = Comp]

dt[DiffCost/DiffLife<=0, .N, by = Comp]
dt[DiffCost/DiffLife>0 & 
      DiffCost/DiffLife<=gdp, .N, by = Comp]
dt[DiffCost/DiffLife>0 & 
      DiffCost/DiffLife>gdp, .N, by = Comp]

dt <- merge(dt, dt[,.(DiffCostMean = predict(lm(DiffCost~1), interval="predict",level = .9)[1,1],
                 DiffCostMin = predict(lm(DiffCost~1), interval="predict",level = .9)[1,2],
                 DiffCostMax = predict(lm(DiffCost~1), interval="predict",level = .9)[1,3]), 
              by = Comp], by = "Comp", all.x = T)

dt <- merge(dt, dt[,.(DiffLifeMean = predict(lm(DiffLife~1), interval="predict",level = .9)[1,1],
                 DiffLifeMin = predict(lm(DiffLife~1), interval="predict",level = .9)[1,2],
                 DiffLifeMax = predict(lm(DiffLife~1), interval="predict",level = .9)[1,3]), 
              by = Comp], by = "Comp", all.x = T)


```

```{r create overall graphic}
dol <- dollar_format(prefix = "$", big.mark = ",")

l <- data.table(V1 = c("1 life year equivalent to 1 GDP per capita",
                       "1 life year equivalent to 1/2 GDP per capita"),
                V2 = c(1194,1194/2))

leg <- get_legend(ggplot(dt) +
                    aes(y = DiffCost, x = DiffLife, color = Comp) +
                    geom_point(alpha = .8) +
  geom_abline(aes(slope = V2,intercept = 0, linetype = V1), data = l, linewidth = .7, 
              color = "blue") +
                    scale_colour_manual(values = c('#377eb8','#4daf4a','#984ea3')) +
                    guides(colour = guide_legend(override.aes = list(size = 5),
                                                 title = "", ncol = 1,order = 1),
                           linetype = guide_legend(title = "",
                                                   nrow = 2, order = 2,
                                                   override.aes = list(size = 5))
                           ) +
                    theme_bw() +
                    theme(text = element_text(size = 18, face = "bold"),
                          legend.margin = margin(0,0,0,0),
                          legend.position = "bottom",
                          legend.justification = "left"))

dt1 <- dt[Comp == "UAV vs Motorcycle",]

g1 <- ggplot(dt1) +
  aes(y = DiffCost, x = DiffLife, color = Comp) +
  geom_abline(aes(slope = V2,intercept = 0, linetype = V1), data = l, linewidth = .7, color = "blue") +
  geom_point(alpha = .08) +
  geom_errorbar(aes(x = DiffLifeMean, y = DiffCostMean), colour = "black",      
                xmin = dt1$DiffLifeMin, 
                xmax = dt1$DiffLifeMax,
                size = .5, width = 500) +
  geom_errorbar(aes(x = DiffLifeMean, y = DiffCostMean), colour = "black",
                ymin = dt1$DiffCostMin, 
                ymax = dt1$DiffCostMax,
              size = .5, width = 2) +
  geom_point(aes(x = DiffLifeMean, y = DiffCostMean), color = "red", alpha =.8) +
  xlab("Years of life gained") +
  ylab("") +
  scale_colour_manual(values = c('#984ea3'))+
  geom_vline(xintercept = 0, linetype = 2, size = .5) +
  geom_hline(yintercept = 0, linetype = 2, size = .5) + 
  scale_y_continuous(breaks = c(0:7*20000),labels = dol(c(0:7*20)*1000),
                     limits = c(0,max(dt1$DiffCost)+30000)) +
  scale_x_continuous(limits = c(min(dt1$DiffLife),max(dt1$DiffLife)),breaks = -3:8*25) +
  guides(color = guide_legend(title = "",label = F), 
         linetype = guide_legend(title = "", nrow = 2, override.aes = list(size = 5))) +
  theme_classic() +
  theme(legend.position = "none",
        legend.background = element_rect(fill='transparent', color=NA),
        legend.text = element_text(size = 15, face = "bold"),
        axis.text  = element_text(size = 16),
        legend.margin = margin(0,0,0,0),
        axis.title = element_text(size = 16, face = "bold"))

dt2 <- dt[Comp != "UAV vs Motorcycle",]

g2 <- ggplot(dt2) +
  aes(y = DiffCost, x = DiffLife, color = Comp) +
  geom_abline(slope = gdp,intercept = 0, alpha = .5, color = "blue") +
  geom_abline(aes(slope = V2,intercept = 0, linetype = V1), data = l, linewidth = .7, color = "blue") +
  geom_point(alpha = .08) +
  geom_errorbar(aes(x = DiffLifeMean, y = DiffCostMean), colour = "black",      
                xmin = dt2$DiffLifeMin, 
                xmax = dt2$DiffLifeMax,
                size = .5, width = 2000) +
  geom_errorbar(aes(x = DiffLifeMean, y = DiffCostMean), colour = "black",
                ymin = dt2$DiffCostMin, 
                ymax = dt2$DiffCostMax,
              size = .5, width = 5) +
  geom_point(aes(x = DiffLifeMean, y = DiffCostMean), color = "red", alpha =.8) +
  xlab("Years of life gained") +
  ylab("Differential cost") +
  scale_colour_manual(values = c('#377eb8','#4daf4a'))+
  geom_vline(xintercept = 0, linetype = 2, size = .5) +
  geom_hline(yintercept = 0, linetype = 2, size = .5) +
  scale_y_continuous(breaks = c(0:8*100000),labels = dol(0:8*100000),
                     limits = c(0,max(dt2$DiffCost)+30000)) +
  scale_x_continuous(limits = c(0,max(dt2$DiffLife)),breaks = 0:10*200) +
  theme_classic() +
  theme(legend.position = "none",
        axis.text  = element_text(size = 16),
        legend.margin = margin(0,0,0,0),
        axis.title = element_text(size = 16, face = "bold"))



```

```{r plot graphic}
# export 1500x1000
plot_grid(g2, g1, leg,labels = c("A.   ","B.   "),label_size = 18,
          rel_widths = c(1.2, 1,0.2),rel_heights = c(.9,.1))
```


```{r cost per test and year of life gained}

ces[,quantile(DroneTotalCost/HIVExposedInfant)]
ces[,quantile(DroneTotalCostWithoutART/HIVExposedInfant)]

ces[,quantile(MBTotalCost/HIVExposedInfant)]
ces[,quantile(MBTotalCostWithoutART/HIVExposedInfant)]

# additional cost per test
ces[,mean((DroneTotalCost-MBTotalCost)/HIVExposedInfant)]
ces[,quantile((DroneTotalCost-MBTotalCost)/HIVExposedInfant)]

ces[,mean((DroneTotalCost-BaselineTotalCost)/HIVExposedInfant)]
ces[,quantile((DroneTotalCost-BaselineTotalCost)/HIVExposedInfant)]

ces[,mean((MBTotalCost-BaselineTotalCost)/HIVExposedInfant)]
ces[,quantile((MBTotalCost-BaselineTotalCost)/HIVExposedInfant)]



```

# sensibility analysis
## drone cost
```{r sensibility analysis with Drone cost}
tab <- data.table(NewCost = c(3000,5000,9999,15000,20000)*
                    (1+mean(ces$DroneLoss) + sum((p[IndicatorCode == "DroneMainCost",Value]+
                              p[IndicatorCode == "DroneAssuranceCost",Value])/
                             ((1+rd)^(0:4)))),
           DronePrice = c(3000,5000,9999,15000,20000),
           DiffLifeUAVMB = sum(ces[, (DroneSurvivalDaysr3-MBSurvivalDaysr3)/365]),
           DiffLifeUAVBaseline = sum(ces[, (DroneSurvivalDaysr3-BaselineSurvivalDaysr3)/365]))

tab[, DiffCostUAVMB := sum(ces[, DroneTotalCost-MBTotalCost]) + (NewCost - tab[DronePrice == 9999,NewCost])*10000]
tab[, DiffCostUAVBaseline := sum(ces[, DroneTotalCost-BaselineTotalCost]) + 
      (NewCost - tab[DronePrice == 9999,NewCost])*10000]

tab[,ICERUAVMB := DiffCostUAVMB/DiffLifeUAVMB]
tab[,ICERUAVBaseline := DiffCostUAVBaseline/DiffLifeUAVBaseline]

# Drone price needed for becoming cost-efficient
a <- ((unique(tab$DiffLife))*gdp - sum(ces[, DroneTotalCost-MBTotalCost]))/10000+tab[DronePrice == 9999,NewCost]

a/(1+sum((p[IndicatorCode == "DroneMainCost",Value]+p[IndicatorCode == "DroneAssuranceCost",Value])/((1+rd)^(0:4))))

```

## ARV cost
```{r}
tab <- data.table(ARVPriceDrop = -4:4*0.25,
           DiffLife = sum(ces[, (DroneSurvivalDaysr3-MBSurvivalDaysr3)/365]),
           DiffCost = sum(ces[, DroneTotalCost-MBTotalCost])+
             sum(ces[,DroneHIVARTCostr3-MBHIVARTCostr3])*(-4:4*0.25))

tab[,ICER := DiffCost/DiffLife]

tab <- data.table(ARVPriceDrop = -4:4*0.25,
           DiffLife = sum(ces[, (DroneSurvivalDaysr3-BaselineSurvivalDaysr3)/365]),
           DiffCost = sum(ces[, DroneTotalCost-BaselineTotalCost])+
             sum(ces[,DroneHIVARTCostr3-BaselineHIVARTCostr3])*(-4:4*0.25))

tab[,ICER := DiffCost/DiffLife]


tab <- data.table(ARVPriceDrop = -4:4*0.25,
           DiffLife = sum(ces[, (MBSurvivalDaysr3-BaselineSurvivalDaysr3)/365]),
           DiffCost = sum(ces[, MBTotalCost-BaselineTotalCost])+
             sum(ces[,MBHIVARTCostr3-BaselineHIVARTCostr3])*(-4:4*0.25))

tab[,ICER := DiffCost/DiffLife]

```

## distance between the centre and the laboratory
```{r data by distance}
# categorical variable
cesy[RoadDistanceKm <5, RoadDistanceKmCat := 1]
cesy[RoadDistanceKm >=5 & RoadDistanceKm <=15, RoadDistanceKmCat := 2]
cesy[RoadDistanceKm >15, RoadDistanceKmCat := 3]

val_labels(cesy$RoadDistanceKmCat) <- c("<5km" = 1,
                                      "5-15 km" = 2,
                                      ">15km" = 3)
# categorical variable
#cesy[RoadDistanceKm <15, RoadDistanceKmCat := 1]
#cesy[RoadDistanceKm >=15, RoadDistanceKmCat := 2]

# create simulation table per year----
ces <- cesy[,.(DroneSurvivalDays = sum(DroneSurvivalDays), 
              DroneSurvivalDaysr3 = sum(DroneSurvivalDaysr3), 
              MBSurvivalDays = sum(MBSurvivalDays), 
              MBSurvivalDaysr3 = sum(MBSurvivalDaysr3), 
              
              DroneHIVCareARTCostr3 = sum(DroneHIVCareARTCostr3),
              MBHIVCareARTCostr3 = sum(MBHIVCareARTCostr3),
              DroneLoss = sum(DroneLoss, na.rm = T),
              
              DroneCostTravel = sum(DroneCostTravel, na.rm = T),
              MBCostTravel = sum(MBCostTravel, na.rm = T),
              MBReplacement = sum(MBReplacement),
              TravelTimeDiff = weighted.mean(TravelTimeDiff, HIVExposedInfant, na.rm = T),
              MBAccidentCost = sum(MBCostTravel, na.rm = T),
              IsInfantHIVInfected = sum(IsInfantHIVInfected),
              HIVExposedInfant = sum(HIVExposedInfant)
              ),
           by = .(SimulationID,Year,RoadDistanceKmCat)]


# add fixed total annual cost with 3% for inflation----
## drone
ces[, DroneTotalAnnualCost := (DroneCostTravel+
                                 p[IndicatorCode == "DronePilotCost",Value]*1.15+
                                 p[IndicatorCode == "DroneMainCost",Value]*p[IndicatorCode == "DroneCost",Value]+
                                 p[IndicatorCode == "DroneAssuranceCost",Value]*p[IndicatorCode == "DroneCost",Value]+
                                 DroneLoss*p[IndicatorCode == "DroneCost",Value]+
                                 p[IndicatorCode == "TelephonePlanCost",Value]*35)/((1+rd)^(Year-1))]


## motorbike
ces[, MBTotalAnnualCost := (MBCostTravel+MBAccidentCost+
                              p[IndicatorCode == "BikeDriverSalary",Value]*1.15+
                              p[IndicatorCode == "BikeInsurance",Value]+
                              p[IndicatorCode == "BikeRegistration",Value]+
                              p[IndicatorCode == "BikeCost",Value]*MBReplacement+
                              p[IndicatorCode == "TelephonePlanCost",Value]*35)/((1+rd)^(Year-1))]

## consider HIV care separately
ces[,DroneHIVCareARTCost := DroneHIVCareARTCostr3*(.97^(Year-1))]
ces[,MBHIVCareARTCost := MBHIVCareARTCostr3/((1+rd)^(Year-1))]

# create final simulation table-----
ces <- ces[,.( DroneSurvivalDays = sum(DroneSurvivalDays),
               DroneSurvivalDaysr3 = sum(DroneSurvivalDaysr3),
               MBSurvivalDays = sum(MBSurvivalDays), 
               MBSurvivalDaysr3 = sum(MBSurvivalDaysr3),
               
               DroneHIVCareARTCost = sum(DroneHIVCareARTCost),
               MBHIVCareARTCost = sum(MBHIVCareARTCost),
               
               DroneTotalCost = sum(DroneTotalAnnualCost, na.rm = T),
               MBTotalCost = sum(MBTotalAnnualCost, na.rm = T)),
            by = .(SimulationID,RoadDistanceKmCat)]

ces[,DroneTotalCost := DroneTotalCost + DroneHIVCareARTCost +
      p[IndicatorCode %in% c("DroneCost","DroneConsoleCost","DroneFlightCaseCost",
                             "DronePiloteTrainingOnsiteCost","DronePiloteTrainingOnlineCost"
                             ),sum(Value)] +
      p[IndicatorCode == "DroneMedicTrainingCost",Value*33*2] +
      p[IndicatorCode == "TelephoneCost",Value]*35]

ces[,MBTotalCost := MBTotalCost + MBHIVCareARTCost +
      p[IndicatorCode %in% c("BikeCost","BikeHelmet"), sum(Value)] +
      p[IndicatorCode == "TelephoneCost",Value]*35]



ces[,DroneTotalCostWithoutART := DroneTotalCost - DroneHIVCareARTCost]
ces[,MBTotalCostWithoutART := MBTotalCost - MBHIVCareARTCost]

ces[, DiffLife := (DroneSurvivalDaysr3-MBSurvivalDaysr3)/365]
ces[, DiffCost := (DroneTotalCost-MBTotalCost)]


# compute ICER
ces[, sum(DroneTotalCost-MBTotalCost)/sum(DiffLife), by = RoadDistanceKmCat]
ces[((DroneTotalCost-MBTotalCost)/DiffLife)>0 & 
      ((DroneTotalCost-MBTotalCost)/DiffLife)>gdp & RoadDistanceKmCat == 2, .N]/100
ces[((DroneTotalCost-MBTotalCost)/DiffLife)<=0 & RoadDistanceKmCat == 2, .N]/100
```

```{r}
dol <- dollar_format(prefix = "$", big.mark = ",")

tab <- ces[, .(LM = mean(DiffLife), 
               LMICL = predict(lm(DiffLife~1), interval="predict",level = .9)[1,2],
               LMICU = predict(lm(DiffLife~1), interval="predict",level = .9)[1,3],
               CM = mean(DiffCost), 
               CMICL = predict(lm(DiffCost~1), interval="predict",level = .9)[1,2], 
               CMICU = predict(lm(DiffCost~1), interval="predict",level = .9)[1,3]), 
           by = RoadDistanceKmCat]

ggplot(tab) +
  geom_abline(slope = 1032,intercept = 0, color = "blue", alpha = .5) +
  annotate(geom="text", x= 110, y = 90000, label = "Cost-effectiveness threshold", 
           color = "black", size = 4.5) +
  geom_point(aes(x = DiffLife, y = DiffCost, color = to_factor(RoadDistanceKmCat)), 
             data = ces, alpha = .03) +
  aes(x = LM, y = CM, color = to_factor(RoadDistanceKmCat)) +
  geom_errorbar(aes(xmin = LMICL, xmax = LMICU), size = .5, width = 2000) +
  geom_errorbar(aes(ymin = CMICL, ymax = CMICU), size = .5, width = 2.5) +
  geom_point() +
  geom_vline(xintercept = 0, linetype = 2, size = .5) +
  geom_hline(yintercept = 0, linetype = 2, size = .5) +
  xlab("Year of life gained by UAV (vs motorcycle)") +
  ylab("Diffrential cost (UAV vs motorcycle)") +
  scale_y_continuous(breaks = c(0:7*20000),labels = dol(c(0:7*20)*1000),
                     limits = c(0,max(ces$DiffCost)+2000)) +
  scale_x_continuous(limits = c(min(ces$DiffLife),max(ces$DiffLife))) +
  guides(color = guide_legend(title = "Road distance between the health centres and the central laboratory",
                              title.position = "top",
                              override.aes = list(size = 3))) +
  theme_classic() +
  theme(text = element_text(size = 16), 
        legend.position = "bottom", 
        legend.justification = c(0,0),
        legend.title = element_text(size = 16),
        legend.text = element_text(size = 16, margin = margin(r = 10, unit = "pt")),
        legend.margin = margin(t = 0))


```

# Differential time between UAV and motocycle by distance
```{r mean time by km}
tab <- cesy[,.(TravelTimeDiff = weighted.mean(TravelTimeDiff, HIVExposedInfant, na.rm = T)),
            by = .(RoadDistanceKm,SimulationID)]

ggplot(tab) +
  aes(x = RoadDistanceKm, y = TravelTimeDiff, group = RoadDistanceKm) +
  geom_boxplot(outlier.size = .2, varwidth = TRUE) +  
  stat_boxplot(width=.3) +
  scale_y_continuous(breaks = -2:7*20) +
  scale_x_continuous(breaks = 0:12*4) +
  xlab("Distance from the central laboratory (km)") +
  ylab("Travel time saved by UAV (vs motorcycle)") +
  geom_hline(yintercept = 0, linetype = "dashed") +
  geom_hline(yintercept = 1:10*10, linetype = "dotted") +
  theme_classic() +
  theme(text = element_text(size = 16), 
        legend.position = "bottom", 
        legend.justification = c(0,0),
        legend.title = element_text(size = 16),
        legend.text = element_text(size = 16, margin = margin(r = 10, unit = "pt")),
        legend.margin = margin(t = 0))
  
```
