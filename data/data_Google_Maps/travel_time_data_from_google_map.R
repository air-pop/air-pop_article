## clear env
rm(list = ls())

## load package
library(gmapsdistance)
library(data.table)
library(readxl)
library(timeDate)
library(lubridate)
library(chron)

## import data on structures
hop <- data.table(read_excel("data/data_health_center.xlsx"))

## selecting only working days
days <- timeSequence(as.Date("2023/01/01"), as.Date("2023/12/31"))
days <- days[isWeekday(days)]
days <- data.table(Day = dayOfWeek(days), Date = days@Data)

## selecting working hours
time <- merge(7:16, c(0,30))
setorder(time,x)
time <- chron(time = paste(time$x, ':', time$y, ':', 0))
time <- as.character(time)

## merging both time and date on a same dataset
sit <- hop[!is.na(RoadDistanceKm),Site]

tab <- data.table()
for (i in c(1:length(sit))) {
  for (j in c(1:length(time))){
    tab <- rbind(tab,
                 cbind(hop[Site == sit[i],.(Site,Lat,Long)],days,time[j])
    )
  }
}

names(tab)[6] <- "TimeGo"

## avoid a bug with 12:00:00 research with googlemap
tab[, TimeGo := gsub("00$", "01", TimeGo)]

setorder(tab,Date)

## put Google API key
#API_KEY <- 

## calculate travel time
for (i in c(7711:nrow(tab))) {
  print(i)
  tab[i, TravelDurationGo := gmapsdistance(origin = "9.53560467667224+-13.6834612853757",
                                           destination = tab[i,paste0(Lat,"+",Long)],
                                           mode = "driving",
                                           key = API_KEY,
                                           traffic_model = "optimistic",
                                           dep_date = as.character(tab[i,Date]),
                                           dep_time = as.character(tab[i,TimeGo]))$Time]
  
  tab[i, TimeReturn := chron(time = TimeGo) + (TravelDurationGo/(24*60*60))]
  
  tab[i, TravelDurationReturn := gmapsdistance(origin = tab[i,paste0(Lat,"+",Long)], mode = "driving",
                                               destination = "9.53588047056431+-13.6836677257156",
                                               key = API_KEY,
                                               traffic_model = "optimistic",
                                               dep_date = as.character(tab[i,Date]),
                                               dep_time = as.character(tab[i,TimeReturn]))$Time]
}

save(tab, file = "data/route_time_data_optimistic.rData")
