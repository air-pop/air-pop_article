# Script for checking automatically dependencies 
# and installing missing packages

if (compareVersion(paste0(R.version$major,".",R.version$minor), "3.3.1") < 0)
  stop("PLEASE UPDATE R (verion 3.3.1 minimum)")

if(!require(devtools))
  install.packages("devtools")
require(devtools)

# 1. Findind missing packages ----------

p <- c()
for (f in list.files(pattern = "Rmd|R$", recursive = TRUE)) {
  t <- readLines(f, warn = FALSE)
  p <- unique(c(p, t[grepl("^(library|require)", t)]))
  
}
p <- gsub("library|require|\\(|\\)", "", p)
p <- gsub("\\ .*", "", p)

# packages not automatically detected
p <- c(p, "rmdformats")

p <- unique(p)
m <- p[!p %in% installed.packages()[, 1]]

if (!length(m)) {
  message("All required packages are installed.")
  
} else {
  message("Missing packages: ", paste0(m, collapse = ", "))
}

# 2. Install missing packages (CRAN or GitHub or Bioconductor) ----------------

if ("graph" %in% m) {
  source("https://bioconductor.org/biocLite.R")
  biocLite("graph")
}

thru_github <- c("questionr", "haven", "ggplot2", "JLutils", "RDS")
repo_github <- c("juba", "hadley", "larmarange")

for (i in m[m %in% thru_github])
  install_github(i, username = repo_github[which(thru_github == i)])

for (i in m[!m %in% thru_github])
  install.packages(i, dependencies = TRUE)

if (any(!p %in% installed.packages()[, 1]))
  stop("Some install failed!")

# 3. Manuel checking of some packages -----------

library(haven)
if (!existsFunction("labelled_spss"))
  devtools::install_github("hadley/haven")

if (compareVersion(as.character(packageVersion("questionr")), "0.5.1") < 0) {
  install.packages("httpuv")
  devtools::install_github("juba/questionr")
}


if (compareVersion(as.character(packageVersion("labelled")), "0.2.3") < 0)
  install.packages("labelled")

