# Air-pop_article
## Description
Datasets and code for the article named: "Improving early infant diagnosis for HIV using micro unmanned aerial vehicles (UAV) versus motorcycles: a cost-effectiveness comparative analysis."

## Instructions
Follow the following steps to set up the build of the initial parameters:
1) run the "checking_dependencies.R" file
2) run the "make_directories.R" file
3) run all the files found in "building parameters"

All other analysis can be then run from that point.

## Authors and acknowledgment
**Code developpement and analysis**: Maxime Inghels (owner) and Paul Mee (contributor)

**Project contributor** (alphabetic order): Zahid Asghar, Guillaume Breton, Mohamed Cissé, Oumou Hawa Diallo, Youssouf Koita, Gabrièle Laborde-Balen, Frank Tanser.
